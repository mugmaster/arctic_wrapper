# -*- coding: utf-8 -*-
"""
Created on Sat Jul 23 15:54:22 2016

"""
import re

def get_arctic_store(server='localhost'):
    from arctic import Arctic
    arctic = Arctic(server)
    return arctic

def get_arctic_lib(lib_name, server='localhost'):
    from arctic import Arctic
    arctic = Arctic(server)
    lib = arctic[lib_name]
    return lib


class arctic_lib:
    def __init__(self, name, server='localhost'):
        if server[0] == ':':
            server = 'localhost'+server
        self.lib = get_arctic_lib(name, server=server)
        self.server = server
    
    def ls(self, pattern='%'):
        pattern = "^" + pattern.replace('%', '.*') + '$'
        return self.lib.list_symbols(regex=pattern)
    
    def read(self, sym, as_of = None):
        if isinstance(sym, list):
            data = {}
            for s in sym:
                if as_of is None:
                    data[s] = self.lib.read(s).data
                else:
                    data[s] = self.lib.read(s, as_of=as_of).data
            return data        
        else:
            if as_of is None:
                return self.lib.read(sym).data
            else:
                return self.lib.read(sym, as_of=as_of).data
                
    def __getitem__(self, substr = '%'):
        if '%' in substr:
            sel_keys = self.ls(substr)
            return self.read(sel_keys)
        else:
            return self.read(substr)
    
    def write(self, sym, data):
        self.lib.write(sym, data)

    def set_quota(self, quota):
        self.lib._arctic_lib.set_quota(quota*1024*1024*1024)
    
    def get_quota(self):
        return self.lib._arctic_lib.get_quota()/(1024*1024*1024)
    def _ipython_key_completions_(self):
        return self.ls()

class lib:
    def __init__(self, name, server='localhost'):
        if server[0] == ':':
            server = 'localhost'+server
        self.lib = get_arctic_lib(name, server=server)
        self.server = server
    
    def ls(self, pattern='%'):
        pattern = "^" + pattern.replace('%', '.*') + '$'
        return self.lib.list_symbols(regex=pattern)
    
    def read(self, sym, as_of = None):
        if isinstance(sym, list):
            data = {}
            for s in sym:
                if as_of is None:
                    data[s] = self.lib.read(s).data
                else:
                    data[s] = self.lib.read(s, as_of=as_of).data
            return data        
        else:
            if as_of is None:
                return self.lib.read(sym).data
            else:
                return self.lib.read(sym, as_of=as_of).data
                
    def __getitem__(self, substr = '%'):
        if '%' in substr:
            sel_keys = self.ls(substr)
            return self.read(sel_keys)
        else:
            return self.read(substr)
    
    def write(self, sym, data):
        self.lib.write(sym, data)
    
    def set_quota(self, quota):
        self.lib._arctic_lib.set_quota(quota*1024*1024*1024)
    
    def get_quota(self):
        return self.lib._arctic_lib.get_quota()/(1024*1024*1024)
    
    def _ipython_key_completions_(self):
        return self.ls()

class arctic_store:
    def __init__(self, server='localhost'):
        if server[0] == ':':
            server = 'localhost'+server
        self.server = server
        self.store = get_arctic_store(server=server)
        self.libraries = self.store.list_libraries()
    
    def ls(self, pattern='%'):
        pattern = "^" + pattern.replace('%', '.*') + '$'
        sel_keys = [key for key in self.libraries if 
                    re.search(pattern.lower(), key.lower()) is not None]
        return sel_keys
        
    def add(self, lib_name, quota=10):
        self.store.initialize_library(lib_name)
        self.libraries = self.store.list_libraries()
        lib = self.get(lib_name)
        lib.lib._arctic_lib.set_quota(quota*1024*1024*1024)
        
    def get(self, lib_name):
        if lib_name in self.libraries:
            return arctic_lib(lib_name, server=self.server)
        else:
            print "{} does not exist on {}".format(lib_name, self.server)

    def __getitem__(self, substr = '%'):
        if '%' in substr:
            sel_keys = self.ls(substr)
            libs = {}
            for s in sel_keys:
                libs[s] = self.get(s)
            return libs
        else:
            return self.get(substr)
    
    def _ipython_key_completions_(self):
        return self.ls()
        


class store:
    def __init__(self, server='localhost'):
        if server[0] == ':':
            server = 'localhost'+server
        self.server = server
        self.store = get_arctic_store(server=server)
        self.libraries = self.store.list_libraries()
    
    def ls(self, pattern='%'):
        pattern = "^" + pattern.replace('%', '.*') + '$'
        sel_keys = [key for key in self.libraries if 
                    re.search(pattern.lower(), key.lower()) is not None]
        return sel_keys
        
    def add(self, lib_name):
        self.store.initialize_library(lib_name)
        self.libraries = self.store.list_libraries()
        return self.store[lib_name]
        
    def get(self, lib_name):
        if lib_name in self.libraries:
            return lib(lib_name, server=self.server)
        else:
            print "{} does not exist on {}".format(lib_name, self.server)
    def __getitem__(self, substr = '%'):
        if '%' in substr:
            sel_keys = self.ls(substr)
            libs = {}
            for s in sel_keys:
                libs[s] = self.get(s)
            return libs
        else:
            return self.get(substr)

    def _ipython_key_completions_(self):
        return self.ls()